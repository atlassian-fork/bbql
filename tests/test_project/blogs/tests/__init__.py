def take(indices, l):
    """Returns specific elements from the supplied list.

    >>> take([1, 0, 0, 3], range(10)) --> 1, 0, 0, 3
    """
    return (l[idx] for idx in indices)
