# -*- coding: UTF-8 -*-
import six

from bbql import FilterException, execute as runbbql
from bbql.exceptions import InvalidPathException
from bbql.orm import QBuilder, get_orm_field_value
from django.contrib.auth.models import User
from django.test import TestCase

from blogs.models import Blog, Comment


class TestFieldResolution(TestCase):
    fixtures = ['test_data.json']

    def test_get_field_value(self):
        self.assertEqual('Sydney',
                         get_orm_field_value(
                             Comment.objects.get(pk=1),
                             tuple('user.location'.split('.'))))

    def test_custom_column_class(self):
        self.assertEqual('published',
                         get_orm_field_value(
                             Comment.objects.get(pk=1),
                             tuple('blog.state'.split('.'))))

    def test_invalid_path(self):
        with self.assertRaises(InvalidPathException):
            get_orm_field_value(Comment.objects.get(pk=1),
                                tuple('does.not.exist'.split('.')))


class TestFiltering(TestCase):
    fixtures = ['test_data.json']

    def setUp(self):
        self.erik = User.objects.get(username='erik')
        self.joe = User.objects.get(username='joe')

    def test_foreign_key(self):
        # one-level nesting: all comments on a blog post:
        qs = runbbql(Comment.objects.all(), 'blog.title="Erik\'s blog"')
        self.assertEqual(2, qs.count())
        six.assertCountEqual(
            self,
            list(Comment.objects.filter(blog__title="Erik's blog")), list(qs))

        # two-level nesting: all comments to a specific blog post author:
        qs = runbbql(Comment.objects.distinct(), 'blog.user.username="erik"')
        self.assertEqual(2, qs.count())
        six.assertCountEqual(
            self,
            list(Comment.objects.filter(blog__title="Erik's blog")), list(qs))

    def test_fieldname_translation(self):
        """Asserts that field names get properly translated between API and
        DB.
        """
        qs = runbbql(Blog.objects.all(), 'user.username="erik"')
        self.assertEqual(1, qs.count())
        self.assertEqual(Blog.objects.get(pk=1), qs[0])

    def test_unknown_field(self):
        try:
            runbbql(Blog.objects.all(), 'foo="bar"')
            self.fail('Invalid field was not rejected')
        except FilterException:
            pass

        try:
            runbbql(Comment.objects.all(), 'blog.user.foo="bar"')
            self.fail('Invalid field was not rejected')
        except FilterException:
            pass

    def test_ignore_errors(self):
        """Assert that invalid field names are can be ignored."""
        expected = list(Comment.objects.order_by('id'))
        q = QBuilder.registry[Comment].q(
            'blog.user.foo.bar'.split('.'), '=', 'erik', '', ignore_invalid_fields=True)

        self.assertListEqual(expected,
                             list(Comment.objects.filter(q).order_by('id')))

    def test_negation(self):
        qs = runbbql(Blog.objects.all(), 'user.username != "erik"')
        self.assertEqual(1, qs.count())
        self.assertEqual(Blog.objects.get(pk=2), qs[0])

    def test_date(self):
        # equality
        qs = runbbql(
            User.objects.all(),
            'created_on = %s' % str(self.erik.date_joined.isoformat()))
        self.assertEqual(1, qs.count())
        self.assertEqual(self.erik, qs[0])

        # greater than
        qs = runbbql(
            User.objects.all(),
            'created_on > %s' % str(self.erik.date_joined.isoformat()))
        self.assertEqual(1, qs.count())
        self.assertEqual(self.joe, qs[0])

    def test_numerical(self):
        qs = runbbql(User.objects.all(), 'id = 2')
        self.assertEqual(1, qs.count())
        self.assertEqual(self.joe, qs[0])

        # less than
        qs = runbbql(User.objects.all(), 'id < 2')
        self.assertEqual(1, qs.count())
        self.assertEqual(self.erik, qs[0])

        # greater than
        qs = runbbql(User.objects.all(), 'id > 1')
        self.assertEqual(1, qs.count())
        self.assertEqual(self.joe, qs[0])

        # less than or equal
        qs = runbbql(User.objects.distinct(), 'id <= 2')
        self.assertEqual(2, qs.count())

        # greater than or equal
        qs = runbbql(User.objects.distinct(), 'id >= 1')
        self.assertEqual(2, qs.count())

    def test_empty_query(self):
        """Assert that an empty query string is ignored and not treated as a
        syntax error.
        """
        qs = runbbql(User.objects.distinct(), '')
        self.assertEqual(2, qs.count())

    def test_string_escaping(self):
        qs = runbbql(Comment.objects.all(),
                     'content = "You call yourself an \\"author\\"?"')
        self.assertEqual(1, qs.count())

        qs = runbbql(Comment.objects.all(),
                     u'content = "You call this an \\"地震\\"?"')
        self.assertEqual(1, qs.count())

        qs = runbbql(Comment.objects.all(),
                     u'content = "You call this \\"водка\\"?"')
        self.assertEqual(1, qs.count())

    def test_field_value_translation(self):
        qs = runbbql(Blog.objects.all(), 'state = "published"')
        self.assertEqual(1, qs.count())
        self.assertEqual(1, qs[0].id)

        qs = runbbql(Blog.objects.all(), 'state = "redacted"')
        self.assertEqual(0, qs.count())

    def test_contains(self):
        self.assertEqual(
            Comment.objects.get(pk=4),
            runbbql(Comment.objects.all(), 'content ~ "Yourself"').get())

    def test_not_contains(self):
        six.assertCountEqual(
            self,
            Comment.objects.exclude(pk=4),
            runbbql(Comment.objects.all(), 'content !~ "Yourself"'))

    def test_null(self):
        self.assertEqual(
            Comment.objects.get(pk=5),
            runbbql(Comment.objects.all(), 'user = null').get())

    def test_not_null(self):
        six.assertCountEqual(
            self,
            list(Comment.objects.exclude(pk=5)),
            list(runbbql(Comment.objects.all(), 'user != null')))

    def test_boolean(self):
        self.assertEqual(
            Blog.objects.get(pk=2),
            runbbql(Blog.objects.all(), 'meta.muted = true').get())

        self.assertEqual(
            Blog.objects.get(pk=2),
            runbbql(Blog.objects.all(), 'meta.muted != false').get())

    def test_and(self):
        six.assertCountEqual(
            self,
            list(Comment.objects.filter(
                blog__author__username='erik', author__username='joe')),
            list(runbbql(
                Comment.objects.all(),
                'blog.user.username = "erik" and user.username = "joe"')))

    def test_case_insensitive_and(self):
        six.assertCountEqual(
            self,
            list(Comment.objects.filter(
                blog__author__username='erik', author__username='joe')),
            list(runbbql(
                Comment.objects.all(),
                'blog.user.username = "erik" And user.username = "joe"')))

    def test_many_to_many(self):
        six.assertCountEqual(
            self,
            list(Blog.objects.filter(pk=2)),
            list(runbbql(Blog.objects.all(), 'likes.username = "erik"')))

    def test_nested_column(self):
        six.assertCountEqual(
            self,
            list(Comment.objects.filter(blog__commenting_closed=True)),
            list(runbbql(Comment.objects.all(), 'meta.muted = true'))
        )

    def test_invalid_syntax(self):
        with self.assertRaises(FilterException):
            runbbql(Comment.objects.all(), 'foo = """')

    def test_one_to_one_field(self):
        six.assertCountEqual(
            self,
            list(User.objects.filter(username='joe')),
            list(runbbql(User.objects.all(), 'location = "Sydney"'))
        )
