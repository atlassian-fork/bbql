from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q

from bbql import filter_orm, filter_iterable
from bbql.orm import Column


PUBLICATION_STATUS = (('1', 'pending'),
                      ('2', 'published'),
                      ('3', 'redacted'))

_encode_status = lambda name: {v: n for n, v in PUBLICATION_STATUS}[name]
_decode_status = lambda number: dict(PUBLICATION_STATUS)[str(number)]


class Blog(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='blogs')
    title = models.CharField(max_length=1024)
    content = models.TextField()
    status = models.CharField(max_length=128, choices=PUBLICATION_STATUS,
                              default='2')
    commenting_closed = models.BooleanField(default=False)
    likes = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                   related_name='liked_blogs')

    def __repr__(self):
        return '<Blog(%d)>' % self.pk

    @property
    def unfiltered_property(self):
        # This field is not explicitly declared in BBQL's mapping and must be
        # invisible to it.
        return None

    @property
    def traffic(self):
        # A property not backed by the ORM.
        return Bytes(100, 200)


class Comment(models.Model):
    blog = models.ForeignKey(Blog, related_name='comments')
    author = models.ForeignKey(settings.AUTH_USER_MODEL, null=True,
                               related_name='comments')
    content = models.TextField()

    def __repr__(self):
        return '<Comment(%d)>' % self.pk


class UserProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                related_name='profile')
    location = models.CharField(max_length=128, blank=True, null=True)

    def __repr__(self):
        return '<UserProfile(%d)>' % self.pk


filter_orm(Blog, (('title', 'title'),
                  ('user', 'author'),
                  ('content', 'content'),
                  ('state', Column(
                      get_value=lambda inst: _decode_status(inst.status),
                      # Custom column name and value translation logic:
                      qs=lambda field, op, v, ormprefix:
                          Q(**{ormprefix + 'status' + op: _encode_status(v)}),
                      # Custom order_by() translation:
                      order_by=lambda qs, direction, field, ormprefix:
                          qs.order_by(direction + ormprefix + 'status'))),
                  ('meta.muted', 'commenting_closed'),
                  ('likes', 'likes'),
                  ('traffic', 'traffic')))

filter_orm(Comment, (('id', 'id'),
                     ('blog', 'blog'),
                     ('user', 'author'),
                     ('content', Column(
                         get_value=lambda inst: inst.content,
                         qs=lambda field, op, v, ormprefix:
                             Q(**{ormprefix + 'content' + op: v}),
                         # Content can be queried, but not sorted on:
                         order_by=None)),
                     ('meta.muted', 'blog__commenting_closed')))

filter_orm(User, (('username', 'username'),
                  ('created_on', 'date_joined'),
                  ('active', 'is_active'),
                  ('location', 'profile__location'),
                  ('id', 'id')))


class Bytes(object):
    def __init__(self, bytes_in, bytes_out):
        self.bytes_in = bytes_in
        self.bytes_out = bytes_out

    def __repr__(self):
        return '<Bytes(%s, %s)>' % (self.bytes_in, self.bytes_out)


class AccessLogEntry(object):
    """A class that represents a data record that isn't backed by an RDBMS."""
    def __init__(self, url, status, bytes_io, attribs=tuple()):
        self.url = url
        self.status = status
        self.bytes = bytes_io
        self.attribs = attribs

    def __repr__(self):
        return '<AccessLogEntry(%s, %s, %s)>' % (self.url, self.status,
                                                 repr(self.bytes))


filter_iterable(AccessLogEntry, (('url', 'url'),
                                 ('status', 'status'),
                                 ('bytes', 'bytes'),
                                 ('kb', lambda e:
                                 Bytes(e.bytes.bytes_in / 1024.0,
                                       e.bytes.bytes_out / 1024.0)),
                                 ('labels', 'attribs')))

filter_iterable(Bytes, (('in', 'bytes_in'),
                        ('out', 'bytes_out')))
