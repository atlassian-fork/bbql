related_obj_types = tuple()

try:
    from django.db.models.fields.related import RelatedObject
    related_obj_types += (RelatedObject,)
    has_rel_obj = True
except ImportError:
    has_rel_obj = False

try:
    from django.db.models.fields.related import ManyToOneRel
    related_obj_types += (ManyToOneRel,)
    has_m2m = True
except ImportError:
    has_m2m = False


def get_related_obj_type(field):
    if has_rel_obj:
        return field.model
    elif has_m2m:
        return field.related_model
    else:
        raise ValueError('Invalid related object')
